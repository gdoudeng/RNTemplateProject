import {fromJS} from 'immutable';
import Types from '../../action/actionTypes';

const defaultState = fromJS({
  userName: '',
  passWord: ''
});

export default (state = defaultState, action) => {
  switch (action.type) {
    case Types.CHANGE_USER_INFO:
      return state.mergeDeep({
        userName: action.userName,
        passWord: action.passWord,
      });
    default:
      return state;
  }
}

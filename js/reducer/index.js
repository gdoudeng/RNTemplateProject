import {combineReducers} from 'redux-immutable';
import Login from './Login';

const index = combineReducers({
  LoginState: Login,
});

export default index;

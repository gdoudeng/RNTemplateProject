import Types from '../actionTypes';


export function onChangeUserInfo(userName, passWord) {
  return dispatch => {
    dispatch({type: Types.CHANGE_USER_INFO, userName, passWord});
  };
}

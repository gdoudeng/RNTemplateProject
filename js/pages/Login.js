import React, {Component} from 'react';
import {View, Text, StyleSheet, TextInput, Button} from 'react-native'
import SplashScreen from "react-native-splash-screen";
import actionCreators from '../action/index';
import {connect} from 'react-redux';
import ProgressHUD from "../component/ProgressHUD";

class Login extends Component {
  componentDidMount() {
    SplashScreen.hide();
  }

  login = () => {
    this.progressHUD.show()
    setTimeout(() => {
      this.progressHUD.hide()
      this.props.navigation.replace('Main')
    }, 2000)
  }

  render() {
    const {userName, passWord, onChangeUserInfo} = this.props
    return (
      <View style={styles.container}>
        <Text>登录页</Text>
        <TextInput
          style={styles.textInput}
          placeholder={'账户'}
          placeholderTextColor="#cacaca"
          keyboardType="number-pad"
          value={userName}
          onChangeText={(username) => onChangeUserInfo(username, passWord)}
        />
        <TextInput
          style={styles.textInput}
          placeholder={'密码'}
          secureTextEntry
          placeholderTextColor="#cacaca"
          keyboardType="number-pad"
          value={passWord}
          onChangeText={(passWord) => onChangeUserInfo(userName, passWord)}
        />
        <Button title={'登录'} onPress={this.login}/>
        <ProgressHUD ref={(ref) => this.progressHUD = ref}/>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  userName: state.getIn(['LoginState', 'userName']),
  passWord: state.getIn(['LoginState', 'passWord']),
});

const mapDispatchToProps = dispatch => ({
  onChangeUserInfo: (userName, passWord) => dispatch(actionCreators.onChangeUserInfo(userName, passWord)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textInput: {
    fontSize: 17,
    borderRadius: 10,
    color: '#000000',
    height: 49,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#000000',
    width: 100,
    marginBottom: 10
  },
})

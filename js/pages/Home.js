import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native'
import {connect} from "react-redux";

class Home extends Component {
  render() {
    const {userName} = this.props
    return (
      <View style={styles.container}>
        <Text>用户名是：{userName}</Text>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  userName: state.getIn(['LoginState', 'userName']),
});

export default connect(mapStateToProps)(Home);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
})

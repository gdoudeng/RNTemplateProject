import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native'
import {connect} from "react-redux";

class Settings extends Component {
  constructor(props) {
    super(props);
    props.navigation.setOptions({
      headerTitle: '设置',
    });
  }

  render() {
    const {passWord} = this.props
    return (
      <View style={styles.container}>
        <Text>密码是：{passWord}</Text>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  passWord: state.getIn(['LoginState', 'passWord']),
});

export default connect(mapStateToProps)(Settings);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
})

import HttpUtils from '../utils/HttpUtils';
import UserDao from "../dao/UserDao";

/**
 * 密码登录
 * @param name
 * @param pwd
 * @returns {Promise}
 */
export function passwdLogin(name, pwd) {
  return HttpUtils.postRequest('/user/passwdLogin', {'password': pwd, 'username': name});
}

export function isLogin() {
  return UserDao.getUserToken()
    .then(token => {
      if (token) {
        HttpUtils.header = {
          'Content-Type': 'application/json',
          'X-FM-SHOOTING-TOKEN': token
        }
      }
      return HttpUtils.postRequest('/security/isLogin');
    })
    .catch(_ => {
      return HttpUtils.postRequest('/security/isLogin');
    })
}

/**
 * 发送验证码
 * @param phone 手机号码
 * @returns {Promise}
 */
export function sendMsg(phone) {
  return HttpUtils.getRequest('/user/sendMsg', {phone});
}

export function logOut() {
  return UserDao.clearUserToken()
    .then(result => {
      HttpUtils.header = {
        'Content-Type': 'application/json',
      }
      return HttpUtils.postRequest('/security/logout');
    })
    .catch(e => {
      HttpUtils.header = {
        'Content-Type': 'application/json',
      }
      return HttpUtils.postRequest('/security/logout');
    })
}

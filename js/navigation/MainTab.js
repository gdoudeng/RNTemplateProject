import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HomeScreen from '../pages/Home';
import SettingsScreen from '../pages/Settings';

const Tab = createBottomTabNavigator();

export default function MainTab() {
  return (
    <Tab.Navigator tabBarOptions={{
      activeTintColor: '#607D8B',
    }}>
      <Tab.Screen name="Home" component={HomeScreen} options={{
        tabBarLabel: '首页',
        tabBarIcon: ({tintColor}) => (
          <Ionicons name='md-home' color={tintColor} size={26}/>
        ),
      }}/>
      <Tab.Screen name="Setting" component={SettingsScreen} options={{
        tabBarLabel: '设置',
        tabBarIcon: ({tintColor}) => (
          <Ionicons name='ios-settings' color={tintColor} size={26}/>
        ),
      }}/>
    </Tab.Navigator>
  )
}
